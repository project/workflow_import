DESCRIPTION
===========
Workflow-import is a port of the module workflow-post-install to Drupal 7,
with the inclussion of a administration user interface to make the use friendlier
and safer.
From the D6 module -> The module only performs one task: to put content that is in the workflow state
of 'No state' into a valid state making it usable in workflows. This refers in
particular to content that existed before you installed the Workflow module.

INSTALLATION
============
1. Place the workflow_import folder in "sites/all/modules".
2. Enable the module under Administer >> Site building >> Modules.

USAGE
=====
Visit Administer >> Configuration >> Workflow >> Workflow import
and select the state of their respective workflows to put the nodes
from each available content type.

AUTHOR
======
Jorge Lopez-Lago (kurkuma)